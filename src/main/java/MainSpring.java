import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by KW on 10/13/2017.
 */
public class MainSpring {
    public static void main(String[] args) {

//        Locale locale = Locale.getDefault();
//        Calendar calendar = GregorianCalendar.getInstance(locale);
//
//        DateFormat formatter = SimpleDateFormat.getInstance();
//        System.out.println(formatter.format(calendar.getTime()));


        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        Calendar calendar = context.getBean("calendar", Calendar.class);
        DateFormat formatter = (DateFormat) context.getBean("formatter");

        System.out.println(formatter.format(calendar.getTime()));

    }
}
//tworzymy beana z uzyciem metody fasbrykujacej getDefault i kalendarz - met. z argumentem lokalizacji
//tworzymy instancje formattera, kalendarz i formatujemy
